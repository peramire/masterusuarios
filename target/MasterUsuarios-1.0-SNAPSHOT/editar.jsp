<%@page import="root.model.entities.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%    
    Usuario usuario = (Usuario) request.getAttribute("usuario");
%>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="clientScript.js"></script>
        <title>Edicion</title>
    </head>
    <body>
        <form  name="form" action="controllerUsuario" method="POST" class="form-horizontal">
            <div class="container">
                <h2>Edición de Usuarios</h2>
                <input type="hidden" name="id" value="<%= usuario.getId()%>" id="id">
                <div class="form-group">
                    <label for="rut" class="control-label col-sm-2">Rut:</label>
                    <div class="col-sm-5">
                        <input name="rut" value="<%= usuario.getRut()%>" required id="rut" class="form-control" oninput="checkRut(this)" placeholder="Ingrese RUT">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre" class="control-label col-sm-2">Nombre:</label>
                    <div class="col-sm-5">
                        <input name="nombre" value="<%= usuario.getNombres()%>" required id="nombre" class="form-control">
                    </div>
                </div>       
                <div class="form-group">
                    <label for="apellido" class="control-label col-sm-2">Apellido:</label>
                    <div class="col-sm-5">
                        <input name="apellido" value="<%= usuario.getApellidos()%>" required id="apellido" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="control-label col-sm-2">Username:</label>
                    <div class="col-sm-5">
                        <input  name="username" value="<%= usuario.getUsuario()%>" required id="username" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-sm-2">Password:</label>
                    <div class="col-sm-5">
                        <input name="password" type="password" readonly="readonly" value="<%= usuario.getPassword()%>" id="password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="container" align="center">
                <button class="btn btn-success" type="submit" name="accion" value="editar">Guardar</button>
                <button class="btn btn-default" type="submit" name="accion" value="volver" formnovalidate>Volver</button>
            </div>
        </form>
    </body>
</html>