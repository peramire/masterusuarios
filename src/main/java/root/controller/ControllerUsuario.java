package root.controller;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.UsuarioDAO;
import root.model.entities.Usuario;

@WebServlet(name = "ControllerUsuario", urlPatterns = {"/controllerUsuario"})
public class ControllerUsuario extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String rut = request.getParameter("rut");
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String accion = request.getParameter("accion");

        UsuarioDAO dao = new UsuarioDAO();
        Usuario usuario = new Usuario();

        switch (accion.trim()) {
            case "guardar":
                try {
                    usuario.setRut(rut);
                    usuario.setNombres(nombre);
                    usuario.setApellidos(apellido);
                    usuario.setUsuario(username);
                    usuario.setPassword(password);
                    dao.create(usuario);

                    List<Usuario> usuarios = dao.findUsuarioEntities();
                    request.setAttribute("usuarios", usuarios);
                    request.getRequestDispatcher("lista.jsp").forward(request, response);
                } catch (Exception ex) {
                    Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "editar":
                try {
                    usuario.setId(Integer.parseInt(id));
                    usuario.setRut(rut);
                    usuario.setNombres(nombre);
                    usuario.setApellidos(apellido);
                    usuario.setUsuario(username);
                    usuario.setPassword(password);
                    dao.edit(usuario);

                    List<Usuario> usuarios = dao.findUsuarioEntities();
                    request.setAttribute("usuarios", usuarios);
                    request.getRequestDispatcher("lista.jsp").forward(request, response);
                } catch (Exception ex) {
                    Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "volver":
                List<Usuario> usuarios = dao.findUsuarioEntities();
                request.setAttribute("usuarios", usuarios);
                request.getRequestDispatcher("lista.jsp").forward(request, response);
                break;
        }
        dao = null;
        usuario = null;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
