package root.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.dao.UsuarioDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.entities.Usuario;

@WebServlet(name = "controllerListado", urlPatterns = {"/controllerListado"})
public class ControllerListado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String action = request.getParameter("accion");
        UsuarioDAO dao = new UsuarioDAO();
        switch (action.trim()) {
            case "editar":
                Usuario usu = new Usuario();
                
                usu = dao.findUsuario(Integer.parseInt(id));
                request.setAttribute("usuario", usu);
                request.getRequestDispatcher("editar.jsp").forward(request, response);
                break;
            case "eliminar":
                List<Usuario> usuarios = new ArrayList<Usuario>();
                try {
                    dao.destroy(Integer.parseInt(id));
                    usuarios = dao.findUsuarioEntities();
                } catch (Exception ex) {
                        Logger.getLogger(ControllerListado.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("usuarios", usuarios);
                request.getRequestDispatcher("lista.jsp").forward(request, response);
                break;
            case "crear":
                default:
                request.getRequestDispatcher("crear.jsp").forward(request, response);
                break;
        }
        dao = null;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
