<%@page import="root.model.entities.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    Usuario usuario = (Usuario) request.getAttribute("usuario");
%>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="clientScript.js"></script>
        <title>Cracion Usuarios</title>
    </head>
    <body>
        <form  name="form" action="controllerUsuario" method="POST" class="form-horizontal">
            <div class="container">
                <h2>Formulario para nuevos Usuarios</h2>

                <div class="form-group">
                    <label for="rut" class="control-label col-sm-2">Rut :</label>
                    <div class="col-sm-5">
                        <input name="rut" value="" required id="rut" class="form-control" oninput="checkRut(this)" placeholder="Ingrese RUT">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nombre" class="control-label col-sm-2">Nombre :</label>
                    <div class="col-sm-5">
                        <input name="nombre" value="" required id="nombre" class="form-control">
                    </div>
                </div>       
                <div class="form-group">
                    <label for="apellido" class="control-label col-sm-2">Apellido :</label>
                    <div class="col-sm-5">
                        <input name="apellido" value="" required id="apellido" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="control-label col-sm-2">Username :</label>
                    <div class="col-sm-5">
                        <input  name="username" value="" required id="username" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-sm-2">Password :</label>
                    <div class="col-sm-5">
                        <input name="password" type="password" value="" required id="password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="container" align="center">
                <button class="btn btn-success" type="submit" name="accion" value="guardar">Grabar</button>
                <button class="btn btn-default" type="submit" name="accion" value="volver" formnovalidate>Volver</button>
            </div>
        </form>
    </body>
</html>