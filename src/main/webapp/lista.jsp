<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="root.model.entities.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Usuario> usuarios = (List<Usuario>) request.getAttribute("usuarios");
    Iterator<Usuario> iterUsuario = usuarios.iterator();
%>

<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista Usuarios</title>
    </head>
    <body>
        <div class="container">
            <h2>Tabla de Usuarios</h2>
            <p>Lista de usuarios registrados en la Base de Datos</p>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Rut</th>    
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Usuario</th>
                        <th>Accion</th>                
                    </tr>
                </thead>
                <tbody>                    
                    <%
                        while (iterUsuario.hasNext()) {
                            Usuario usu = iterUsuario.next();
                    %>
                    <tr>
                        <td><%= usu.getRut()%></td>
                        <td><%= usu.getNombres()%></td>
                        <td><%= usu.getApellidos()%></td>
                        <td><%= usu.getUsuario()%></td>
                        <td>
                            <form name="form2" action="controllerListado" method="POST">
                                <input type="hidden" name="id" value="<%= usu.getId()%>"/>
                                <button class="btn btn-info" type="submit" name="accion" value="editar">Editar</button>
                                <button class="btn btn-danger" type="submit" name="accion" value="eliminar" onclick="return confirm('¿Desea Eliminar el Registro Seleccionado?');">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    <%
                        }
                    %>                    
                </tbody>
            </table>
        </div>
        <div class="container" align="center">
            <form name="form" action="controllerListado" method="POST">
                <button class="btn btn-primary" type="submit" name="accion" value="crear">Crear</button>
            </form>
        </div>
    </body>
</html>
